import { sveltekit } from '@sveltejs/kit/vite';
import {NodeGlobalsPolyfillPlugin} from "@esbuild-plugins/node-globals-polyfill";

const config = {
	plugins: [sveltekit()],
	// optimizeDeps: {
	// 	esbuildOptions: {
	// 		define: {
	// 			global: 'globalThis'
	// 		},
	// 		plugins: [
	// 			NodeGlobalsPolyfillPlugin({
	// 				buffer: true
	// 			})
	// 		]
	// 	}
	// }
	// ssr: {
	// 	noExternal: ['rsocket-core']
	// }
};

export default config;
