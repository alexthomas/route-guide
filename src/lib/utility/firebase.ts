// Import the functions you need from the SDKs you need
import {initializeApp} from "firebase/app";
import {browser} from '$app/environment'
import {getFirestore} from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCdMIEc3vfvH-C42l0u5MEMr--a-UYMsIU",
    authDomain: "route-guide-2fb60.firebaseapp.com",
    projectId: "route-guide-2fb60",
    storageBucket: "route-guide-2fb60.appspot.com",
    messagingSenderId: "423327218651",
    appId: "1:423327218651:web:b2a6a558ec41464f2305fe"
};

// Initialize Firebase

const provider = {app: {}, db: {}};

if (browser) {
    const app = initializeApp(firebaseConfig);
    provider.app = app;
    provider.db = getFirestore(app);
}

export const app = provider.app;
export const db = provider.db;

